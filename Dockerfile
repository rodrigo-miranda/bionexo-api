# Set linux Alpine 3.7 image with ruby 2.5 installed
# Others oficial ruby images: https://hub.docker.com/_/ruby/
FROM ruby:2.5-alpine3.7

# Install linux packages
RUN apk add --update --no-cache \
      build-base \
      nodejs \
      tzdata \
      libxml2-dev \
      libxslt-dev \
      bash \
      postgresql-dev \
      mysql-dev

# Set rails env variable
ARG rails_env_var=development
# Application path inside container
ENV APP_ROOT /app

# Create application folder
RUN mkdir $APP_ROOT
# Set command execution path
WORKDIR $APP_ROOT

# Copy files to application folder
ADD Gemfile $APP_ROOT/Gemfile
ADD Gemfile.lock $APP_ROOT/Gemfile.lock

# Install gems
RUN RAILS_ENV=$rails_env_var bundle install --path /bundle

# Copy all project files to application folder inside container
COPY . $APP_ROOT
