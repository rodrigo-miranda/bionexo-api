Bionexo Test
=========

An API for UBS search .

## Challenge
* [Description](01-back-end.pdf)

## Technologies / Dependencies

- Ruby 2.5.1
- Rails-Api 5.2
- Rspec 3.8
- MySQL
- Docker

## To Do List / Improvements

- [ ] Improve ubs import (rake) performance
- [ ] Implement authentication
- [ ] Implement client


## How to run

To install, clone this repository:

```sh
$ git clone https://bitbucket.org/rodrigo-miranda/bionexo-api
$ cd bionexo-api
```
After that, run the service using the folowing command: (Builds Docker images and run it)
```sh
$ ./exec.dev.sh
```

**Note: Wait a few minutes to access API, the initial setup process includes CSV file importing process.**


Example of requests:

```sh
curl --include \
     --request GET \
     --header "Content-Type: application/json" \
'http://localhost:3000/v1/find_ubs?query=-23.604936,-46.692999&page=1&per_page=1'

```

## Automated tests with Rspec
You can run all tests using:

```sh
    $ docker-compose run app bundle exec rspec
```