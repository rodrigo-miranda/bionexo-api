# frozen_string_literal: true

module V1
  class UbsController < ApplicationController

    GEO_REGEX = /^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/
    NUM_REGEX = /^\d+$/

    before_action :validate_params

    def index
      ubs = Ubs.search(params[:lat], params[:long]).paginate(page: params[:page], per_page: params[:per_page])
      render json: ubs, serializer: UbsSerializer, status: :ok
    end

    private

    def validate_params
      geo_params
      paginate_params
    end

    def paginate_params
      if params[:page].present?
        raise ArgumentError, 'page parameter should be a positive integer number' unless NUM_REGEX.match?(params[:page])
      else
        params[:page] = 1
      end

      if params[:per_page].present?
        unless NUM_REGEX.match?(params[:per_page])
          raise ArgumentError, 'per_page parameter should be a positive integer number'
        end
      else
        params[:per_page] = 10
      end
    end

    def geo_params
      if params[:query].present?
        if GEO_REGEX.match?(params[:query])
          geo = params[:query].split(',')
          params[:lat] = geo[0]
          params[:long] = geo[1]
        else
          raise ArgumentError, 'query parameter invalid'
        end
      else
        raise ArgumentError, 'query parameter is mandatory'
      end
    end

  end
end
