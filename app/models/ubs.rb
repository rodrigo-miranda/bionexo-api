# frozen_string_literal: true

class Ubs < ApplicationRecord

  validates :vlr_latitude, :vlr_longitude, :nom_estab, :dsc_endereco,
            :dsc_cidade, :dsc_telefone, :dsc_estrut_fisic_ambiencia,
            :dsc_adap_defic_fisic_idosos, :dsc_equipamentos, :dsc_medicamentos, presence: true

  def self.search(lat, long)
    haversine_sql = "*, (
                6371 * ACOS(
                            COS(RADIANS(#{lat}))
                          * COS(RADIANS(vlr_latitude))
                          * COS(RADIANS(vlr_longitude) - RADIANS(#{long}))
                          + SIN(RADIANS(#{lat}))
                          * SIN(RADIANS(vlr_latitude))
                          )
              ) AS distance"
    select(haversine_sql)
      .order('distance')
  end

  def dsc_estrut_fisic_ambiencia=(value)
    super(score_by_description(value))
  end

  def dsc_adap_defic_fisic_idosos=(value)
    super(score_by_description(value))
  end

  def dsc_equipamentos=(value)
    super(score_by_description(value))
  end

  def dsc_medicamentos=(value)
    super(score_by_description(value))
  end

  private

  def score_by_description(value)
    # Remove extra white spaces
    desc = value.squeeze
    case desc
    when 'Desempenho mediano ou um pouco abaixo da média'
      return 1
    when 'Desempenho acima da média'
      return 2
    when 'Desempenho muito acima da média'
      return 3
    else
      return 0
    end
  end

end
