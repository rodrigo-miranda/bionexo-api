# frozen_string_literal: true

class UbsSerializer < ActiveModel::Serializer

  attributes :current_page, :per_page, :total_entries, :entries

  delegate :current_page, to: :object

  delegate :per_page, to: :object

  delegate :total_entries, to: :object

  def entries
    object.entries.map do |ubs|
      {
        id: ubs.id,
        name: ubs.nom_estab,
        address: ubs.dsc_endereco,
        city: ubs.dsc_cidade,
        phone: ubs.dsc_telefone,
        geocode: { lat: ubs.vlr_latitude, long: ubs.vlr_longitude },
        scores: {
          size: ubs.dsc_estrut_fisic_ambiencia, adaptation_for_seniors: ubs.dsc_adap_defic_fisic_idosos,
          medical_equipment: ubs.dsc_equipamentos, medicine: ubs.dsc_medicamentos
        }
      }
    end
  end

end
