# frozen_string_literal: true

class CreateUbs < ActiveRecord::Migration[5.2]

  def change
    create_table :ubs do |t|
      t.decimal :vlr_latitude, precision: 15, scale: 13, null: false
      t.decimal :vlr_longitude, precision: 16, scale: 13, null: false
      t.integer :cod_munic
      t.integer :cod_cnes
      t.string :nom_estab, null: false
      t.string :dsc_endereco, null: false
      t.string :dsc_bairro
      t.string :dsc_cidade, null: false
      t.string :dsc_telefone, null: false
      t.integer :dsc_estrut_fisic_ambiencia, limit: 1, null: false
      t.integer :dsc_adap_defic_fisic_idosos, limit: 1, null: false
      t.integer :dsc_equipamentos, limit: 1, null: false
      t.integer :dsc_medicamentos, limit: 1, null: false
      t.timestamps
    end
  end

end
