#!/bin/bash

envApplication='development'
dockerfilePath='Dockerfile'
containerRepositoryName='bionexo-api'

# Create container application image with "latest" tag, set "Dockerfile" path,
# change "rails_env_var" variable inside Dockerfile and context path
docker build -t $containerRepositoryName:latest -f $dockerfilePath --build-arg rails_env_var=$envApplication ./

# Execute docker compose to start ala services described in "docker-compose.yml"
# Adding "-d" hide logs. To show logs execute "docker-compose logs"
docker-compose up -d
