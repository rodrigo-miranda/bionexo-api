# frozen_string_literal: true

require 'csv'

namespace :import do
  desc 'Import ubs from csv'

  task ubs: :environment do
    filename = File.join Rails.root, 'ubs.csv'
    CSV.foreach(filename, headers: true) do |row|
      Ubs.create!(row.to_hash)
    end
  end
end
