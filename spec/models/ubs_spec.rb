# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Ubs, type: :model do
  # Validation tests
  # ensure columns are present before saving
  it { should validate_presence_of(:vlr_latitude) }
  it { should validate_presence_of(:vlr_longitude) }
  it { should validate_presence_of(:nom_estab) }
  it { should validate_presence_of(:dsc_endereco) }
  it { should validate_presence_of(:dsc_cidade) }
  it { should validate_presence_of(:dsc_telefone) }

  score1_desc = 'Desempenho mediano ou um pouco abaixo da média'
  score2_desc = 'Desempenho acima da média'
  score3_desc = 'Desempenho muito acima da média'

  ubs_h = { vlr_latitude: -23.604936,
            vlr_longitude: -46.692999,
            nom_estab: 'UBS REAL PQ PAULO MANGABEIRA ALBERNAZ FILHO',
            dsc_endereco: 'RUA BARAO MELGACO',
            dsc_cidade: 'São Paulo',
            dsc_telefone: '1137582329',
            dsc_estrut_fisic_ambiencia: score1_desc.to_s,
            dsc_adap_defic_fisic_idosos: score2_desc.to_s,
            dsc_equipamentos: score3_desc.to_s,
            dsc_medicamentos: score1_desc.to_s }

  subject(:subject) do
    described_class.new(ubs_h)
  end

  context 'Valid scores ' do
    it 'returns correct value for score 1' do
      expect(subject.dsc_estrut_fisic_ambiencia).to eq(1)
    end
    it 'returns correct value for score 2' do
      expect(subject.dsc_adap_defic_fisic_idosos).to eq(2)
    end
    it 'returns correct value for score 3' do
      expect(subject.dsc_equipamentos).to eq(3)
    end
  end

  context '#search' do
    it 'returns correct distance - haversine' do
      result = Ubs.search(-23.604936, -46.692999)
      expect(result.first.distance).to eq(1.4143020925314242)
    end
  end
end
