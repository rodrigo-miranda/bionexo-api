# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'UBS API', type: :request do
  let(:query) { '-23.665185,-46.669398' }
  let(:page) { 1 }
  let(:per_page) { 10 }

  describe 'GET /v1/find_ubs' do
    before { get "/v1/find_ubs?query=#{query}&page=#{page}&per_page=10" }

    context 'valid response' do
      it 'returns ubs ' do
        expect(json).not_to be_empty
        expect(json['entries'].size).to eq(10)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'invalid query param' do
      let(:query) { '-1a,2' }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a error message' do
        expect(response.body).to match(/query parameter invalid/)
      end
    end

    context 'invalid paginate params' do
      let(:page) { 'a' }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a error message' do
        expect(response.body).to match(/page parameter should be a positive integer number/)
      end
    end
  end
end
