#!/bin/bash

# wait for mysql
echo "### Please wait for MySQL start ###"
sleep 15

# initial setup
echo "### Please wait while database install and import Ubs, This may take several minutes ###"
bundle exec rails db:drop db:create db:migrate import:ubs

echo "### Starting Puma web server ###"
# run puma
bundle exec puma -C config/puma.rb
